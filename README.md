# Models

## Usage

Include file in your plantuml with following

### Sequence PlantUML:

```
@startuml myTitle
!include https://gitlab.com/archi-at-sav/models/-/raw/main/plantuml/sequence.puml

...

@enduml
```

### C4 PlantUML:

```
@startuml myTitle
!include https://gitlab.com/archi-at-sav/models/-/raw/main/plantuml/c4.puml

...

@enduml
```

